========================
zhu2020
========================

.. {# pkglts, doc

.. image:: https://b326.gitlab.io/zhu2020/_images/badge_pkging_pip.svg
    :alt: PyPI version
    :target: https://pypi.org/project/zhu2020/1.1.0/

.. image:: https://b326.gitlab.io/zhu2020/_images/badge_pkging_conda.svg
    :alt: Conda version
    :target: https://anaconda.org/revesansparole/zhu2020

.. image:: https://b326.gitlab.io/zhu2020/_images/badge_doc.svg
    :alt: Documentation status
    :target: https://b326.gitlab.io/zhu2020/

.. image:: https://badge.fury.io/py/zhu2020.svg
    :alt: PyPI version
    :target: https://badge.fury.io/py/zhu2020

.. #}
.. {# pkglts, glabpkg_dev, after doc

main: |main_build|_ |main_coverage|_

.. |main_build| image:: https://gitlab.com/b326/zhu2020/badges/main/pipeline.svg
.. _main_build: https://gitlab.com/b326/zhu2020/commits/main

.. |main_coverage| image:: https://gitlab.com/b326/zhu2020/badges/main/coverage.svg
.. _main_coverage: https://gitlab.com/b326/zhu2020/commits/main

prod: |prod_build|_ |prod_coverage|_

.. |prod_build| image:: https://gitlab.com/b326/zhu2020/badges/prod/pipeline.svg
.. _prod_build: https://gitlab.com/b326/zhu2020/commits/prod

.. |prod_coverage| image:: https://gitlab.com/b326/zhu2020/badges/prod/coverage.svg
.. _prod_coverage: https://gitlab.com/b326/zhu2020/commits/prod
.. #}

Data and formalisms from Zhu 2020

