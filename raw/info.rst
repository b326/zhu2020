variety: Sauvignon blanc
site: Marlborough
row_spacing: 2.4
inrow_spacing: 1.8
training: vsp
wire_low: 0.9  # from the ground
wire_high: 1.1  # from the ground
two canes => 20 buds
four canes => 40 buds

sites:

  - Upper Brancott (UB) 41.56569 S; 173.85154 E
  - Western Wairau (WW) 41.51113 S; 173.77012 E
  - Seaview Awatere (SA) 41.62863 S; 174.12950 E
  - Central Rapaura (CR) 41.47617 S; 173.88981 E

  - Glasneven (Canterbury) 43.10592 S; 172.74163 E (3 canes pruned)


years: 2004-2019


stations:

  - UB [41.54248 S; 173.84736 E],
  - WW [41.51343 S; 173.75983 E],
  - SA [41.62959 S; 174.13096 E],
  - CR [41.49137 S, 173.8891 E]


rainfall (mean annual total between 2002 and 2019):

  - UB 684 mm,
  - WW 910 mm,
  - SA 610 mm,
  - CR 723 mm


annual ETo: 1022.6 mm

soil:

  - UB loam,
  - WW loam,
  - SA silty loam,
  - CR silty loam over sandy loam

soil capacity: 80 mm to 140 mm per meter

Seasonal irrigation: 200 to 450 mm
predawn during growing season: -0.1 to -0.4 MPa

leaf area max:

  - 2 canes: 7 m2
  - 4 canes: 10 m2


yield:

  - 2 canes: 5.1 kg.plant-1
  - 4 canes: 7.9 kg.plant-1

bunch number:

  - 2 canes: 39.3
  - 4 canes: 64.7

berry number: 63.4
berry mass: 1.99



yield:

  - 2 canes: yield increase by 0.53 kg if TmaxIni and TmaxFlow increase by 1 [°C]
  - 4 canes: yield increase by 1.29 kg if TmaxIni and TmaxFlow increase by 1 [°C]
