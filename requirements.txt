# {# pkglts, reqs
# requirements are managed by pkglts, do not edit this file at all
# edit .pkglts/pkg_cfg instead
# section reqs

# doc
sphinx
sphinx-gallery
sphinx_rtd_theme

# dvlpt
twine

# install

# script
pandas

# test
coverage
pytest
pytest-cov
pytest-mock

# #}
