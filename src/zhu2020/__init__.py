"""
Data and formalisms from Zhu 2020
"""
# {# pkglts, src
# FYEO
# #}
# {# pkglts, version, after src
from . import version

__version__ = version.__version__
# #}
# {# pkglts, glabdata, after version
from .info import *
# #}
